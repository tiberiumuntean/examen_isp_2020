import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

public class s2 extends JFrame {
    JLabel textLabel, fileLabel;
    JTextField textFieldFile;
    JTextArea textArea;
    JButton buttonSave;

    s2(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(230,250);
        setVisible(true);
    }
    public void init(){
        this.setLayout(null);
        int width = 90;
        int height = 30;

        fileLabel = new JLabel("Numele fisierului");
        fileLabel.setBounds(10, 110, width, height);

        textLabel = new JLabel("Text de salvat");
        textLabel.setBounds(20, 10, width, height);

        textFieldFile = new JTextField();
        textFieldFile.setBounds(20, 140, 180, height);

        buttonSave = new JButton("Salveaza");
        buttonSave.setBounds(20,170, 180, height);

        buttonSave.addActionListener(new s2.SaveActions());

        textArea = new JTextArea();
        textArea.setBounds(10, 40, 170, 60);

        add(textLabel); add(fileLabel);
        add(textFieldFile);
        add(buttonSave);
        add(textArea);
    }


    class SaveActions implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String textInput = s2.this.textArea.getText();
            String fileInput = s2.this.textFieldFile.getText();

            try {
                FileWriter fw = new FileWriter(fileInput);
                fw.write(textInput);
                s2.this.textArea.setText("");
                s2.this.textFieldFile.setText("");
                System.out.println("Fisier creat!");
                fw.close();
            } catch (IOException err){
                System.out.println("Eroare");
                err.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new s2();
    }
}
